<?php
// HIDE ADMIN
// HIDE TOOLBAR
function userRedirect(){
  global $current_user;
  get_currentuserinfo();
  $user_id = $current_user->ID;
  $url=get_bloginfo('url');
  $account=$url."/account";
  if($user_id !== 1){
    show_admin_bar(false);
    if(is_admin()){
      // IF ADMIN IS AN AJAX CALL LET IT BE ELSE REDIRECT TO ACCOUNT.. 
      if (defined('DOING_AJAX') && DOING_AJAX) { 
        // AJAX CALL, LET IT BE..
      } else {
        wp_redirect($account);
        exit;
      }
    }
  };
};
add_action('wp_loaded','userRedirect');
?>
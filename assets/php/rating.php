<?php
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// CREATE DB TABLES

function mjRatingResults() {
  global $wpdb;
  $table_name = $wpdb->prefix."mj_rating_results";
  if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
    // Create new table
    $sql = "CREATE TABLE $table_name (
      id int(11) NOT NULL AUTO_INCREMENT,
      post_id varchar(11) NOT NULL,
      user_id varchar(11) NOT NULL,
      rating_id varchar(11) NOT NULL,
      result TEXT,
      UNIQUE KEY id (id)
    )";
    require_once(ABSPATH.'wp-admin/includes/upgrade.php');
    dbDelta($sql);
  };
};
function mjRatingItems() {
  global $wpdb;
  $table_name = $wpdb->prefix."mj_rating_items";
  if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
    // Create new table
    $sql = "CREATE TABLE $table_name (
      id int(11) NOT NULL AUTO_INCREMENT,
      title varchar(255) NOT NULL,
      description text,
      type_name varchar(55) NOT NULL,
      type_description varchar(255) NOT NULL,
      post_id varchar(55) NOT NULL,
      active varchar(1) NOT NULL,
      UNIQUE KEY id (id)
    )";
    require_once(ABSPATH.'wp-admin/includes/upgrade.php');
    dbDelta($sql);
  };
};
function mjRatingRadios() {
  global $wpdb;
  $table_name = $wpdb->prefix."mj_rating_radios";
  if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
    // Create new table
    $sql = "CREATE TABLE $table_name (
      id int(11) NOT NULL AUTO_INCREMENT,
      rating_id varchar(255) NOT NULL,
      name varchar(255) NOT NULL,
      value varchar(255) NOT NULL,
      UNIQUE KEY id (id)
    )";
    require_once(ABSPATH.'wp-admin/includes/upgrade.php');
    dbDelta($sql);
  };
};
add_action('admin_menu', 'mjRatingItems');
add_action('admin_menu', 'mjRatingResults');
add_action('admin_menu', 'mjRatingRadios');

// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// RATING SECTION ADMIN

function ratingSettingsMenuOptions() {
  add_menu_page('Ratings', 'Ratings', 'administrator', 'ratings', 'ratingSettingsPageOptions', 'dashicons-testimonial', null);
};
function ratingSettingsPageOptions() {
  // DIES IF CURRENT IS NOT ADMIN
  if (!current_user_can('manage_options')) {
    wp_die(__('You do not have sufficient permissions to access this page.'));
  };
  
  // WP DB
  global $wpdb;
  
  // TABLE NAME
  $table = $wpdb->prefix.'mj_rating_items';
  $radioTable = $wpdb->prefix.'mj_rating_radios';
  
  // BASE URL
  $baseUrl = get_admin_url().'/admin.php?page=ratings';
  
  // TYPES
  $types = array(
    array(
      'name' => 'slider',
      'desc' => 'return a value in percent.'
    ),
    array(
      'name' => 'radio',
      'desc' => 'return a value from several options'
    ),
    array(
      'name' => 'text',
      'desc' => 'return some textual comment.'
    ),
  );
  
  // SAVE
  if (isset($_POST['save'])) {
    // SAVE RATING ITEM OPTIONS
    $wpdb -> update (
      $table,
      array(
        'title'            => $_POST['title'],
        'description'      => $_POST['description'],
        'type_name'        => $_POST['type_name'],
        'type_description' => getTypeDescription($_POST['type_name'],$types),
        'active'           => isset($_POST['active']) ? 1 : 0,
        'post_id'          => 0,
      ),
      array('id' => $_POST['id'])
    );
    // SAVE RADIO
    if (isset($_POST['radioId'])) {
      for ($i = 0; $i < count($_POST['radioId']); $i++ ) {
        $wpdb -> update (
          $radioTable,
          array(
            'name'      => $_POST['radioName'][$i],
            'value'     => $_POST['radioValue'][$i],
            'rating_id' => $_GET['edit']
          ),
          array('id' => $_POST['radioId'][$i])
        );
      }
    }
  }
  
  // ADD NEW RATING ITEM
  if (isset($_GET['addRatingItem'])) {
    $type = reset($types);
    $wpdb->insert( $table, array(
        'title'            => 'New Rating Item',
        'type_name'        => $type['name'],
        'type_description' => $type['desc'],
        'post_id'          => 0,
        'active'           => 0,
      )
    );
  }
  
  // ADD NEW RADIO
  if (isset($_GET['addRadio'])) {
    $wpdb->insert($radioTable, array(
        'rating_id' => $_GET['addRadio'],
        'name' => 'default',
        'value' => 'default',
      )
    );
  }
  
  // DELETE RATING ITEM
  if (isset($_GET['deleteRatingItem'])) {
    $wpdb->delete($table, array('ID' => $_GET['deleteRatingItem']));
  }
  
  // DELETE RADIO
  if (isset($_GET['deleteRadio'])) {
    $wpdb->delete($radioTable, array('ID' => $_GET['deleteRadio']));
  }
  
  // EDIT RATING ITEM
  if (isset($_GET['edit'])) {
    $id = $_GET['edit'];
    $arr = getDbResults($table,'id',$id);
    $arr = reset($arr);
    $options = null;
    $selected_type = $arr->type_name;

    foreach ($types as $type) {
      $type = $type['name'];    
      $options .= '<option value="'.$type.'" '.isSelected($type, $selected_type).'>'.$type.'</option>';
    };
    
    $ratingItemOptions = '<div>
      <h1 style="padding-bottom:15px">Edit Rating Item</h1>
      <input type="hidden" name="id" value="'.$id.'" />
      <table class="widefat fixed striped">
        <tbody>
          <tr>
            <td>
              <strong>Title</strong>
            </td>
            <td>
              <input class="regular-text" type="text" name="title" value="'.$arr->title.'" style="width:100%;" />
            </td>
          </tr>
          <tr>
            <th>
              <strong>Description</strong>
            </th>
            <td>
              <textarea cols="30" rows="5" name="description" style="width:100%;">'.$arr->description.'</textarea>
            </td>
          </tr>
          <tr>
            <th>
              <strong>Active</strong>
            </th>
            <td>
              <input type="checkbox" name="active" '.isChecked($arr->active).' />
            </td>
          </tr>
          <tr>
            <th>
              <strong>Type</strong>
            </th>
            <td>
              <select name="type_name" style="width:100%;" onchange="onSelectChange();">'.$options.'</select>
              <input type="hidden" name="type_description" value="'.getTypeDescription($selected_type,$types).'" />
              <strong>'.$selected_type.' '.getTypeDescription($selected_type,$types).'</strong>
            </td>
          </tr>
        </tbody>
      </table>
    </div>';
    
    if ($selected_type == 'radio') {
      $table = $wpdb->prefix.'mj_rating_radios';
      $arr = getDbResults($table,'rating_id',$id);

      if (!$arr) {
        for ($i=0; $i<2; $i++) {
          $wpdb -> insert($table, 
            array(
              'name'      => 'default',
              'value'     => 'default',
              'rating_id' => $id
            )
          );
        };
        $arr = getDbResults($table,'rating_id',$id);
      }
      
      $rows = null; 
      $i = 0;
      foreach ($arr as $item) {
        $i++;
        if ($i > 2) {
          $href = 'href="?page=ratings&edit='.$id.'&deleteRadio='.$item->id.'"';
          $disabled = '';
        } else {
          $href = '';
          $disabled = 'disabled';
        }
        
        $rows .= '<tr>
          <td>
            <input type="text" name="radioName[]" value="'.$item->name.'">
          </td>
          <td>
            <input type="text" name="radioValue[]" value="'.$item->value.'">
          </td>
          <td>
            <input type="hidden" name="radioId[]" value="'.$item->id.'" />
            <a class="button button-secondary button-small '.$disabled.'" '.$href.'/>
              delete
            </a>
          </td>
        </tr>';
      }

      $ratingItemOptions .= '<div>
        <h1 style="padding-bottom:15px">Edit Radio Options</h1>
        <table class="widefat fixed striped">
          <thead>
            <th>name</th>
            <th>value</th>
            <th>delete</th>
          </thead>
          <tbody>
            '.$rows.'
          </tbody>
          <tfoot>
            <tr>
              <td colspan="3">
                <a href="'.$baseUrl.'&edit='.$id.'&addRadio='.$id.'" class="button button-primary button-large">Add</a>
              </td>
            </tr>
          </tfoot>
        </table>
      </div>';
    }
    
    $ratingItemOptions .= '<div>
      <input type="submit" class="button button-primary button-large" value="save" name="save" id="save" style="padding: 0 40px; margin-top: 40px" />
    </div>';
    
    echo '<div>
      <form method="post" action="'.$baseUrl.'&edit='.$id.'">
        <div class="wrap">
          '.$ratingItemOptions.'
        </div>
      </form>
      <script>function onSelectChange(){document.getElementById("save").click();}</script>
    </div>';
  }
  
  // LIST ALL RATING ITEMS (LANDING)
  else {
    $items = $wpdb->get_results("SELECT * FROM $table");
    $row = null;
    
    if ($items) {
      foreach ($items as $item) {
        $row .= '<tr>
          <td class="title column-title has-row-actions column-primary">
            <a class="row-title" href="'.$baseUrl.'&edit='.$item->id.'">
              <strong>'.$item->title.'</strong>
            </a>
            <div class="row-actions">
              <span class="edit">
                <a href="'.$baseUrl.'&edit='.$item->id.'">Edit</a>
              </span>
              <span> | </span>
              <span class="trash">
                <a class="submitdelete" href="'.$baseUrl.'&deleteRatingItem='.$item->id.'">
                  Delete
                </a>
              </span>
            </div>
          </td>
          <td>
            <p>'.$item->type_description.'</p>
          </td>
          <td>
            <p>'.$item->type_name.'</p>
          </td>
          <td>
            <p>'.booleanToValue($item->active,'active','unactive').'</p>
          </td>
        </tr>';
      }
    }
    
    echo '<div class="wrap">
      <h1 style="padding-bottom:15px">Default Rating Items List</h1>
      <form method="post" action="'.$baseUrl.'">
        <table class="wp-list-table widefat fixed striped" cellspacing="0">
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Type</th>
              <th>Active</th>
            </tr>
          </thead>
          <tbody>'.$row.'</tbody>
          <tfoot>
            <tr>
              <td colspan="4">
                <a href="'.$baseUrl.'&addRatingItem" class="button button-primary button-large">Add</a>
              </td>
            </tr>
          </tfoot>
        </table>
      </form>
    </div>';
  }  
};
function getTypeDescription($selected,$items) {
  for ($i = 0; $i < count($items); $i++ ) {
    if ($items[$i]['name'] == $selected) {
      return $items[$i]['desc'];
    }
  }
}
function isSelected($selected,$value) {
  if ($selected == $value) {
    return 'selected="selected"';
  };
};
function isChecked($value) {
  if ($value == 1) {
    return 'checked="checked"';
  };
};
function booleanToValue($bool,$true,$false) {
  if ($bool == 1) { 
    return $true; 
  } else { 
    return $false; 
  }
}
function getDbResults($table,$where,$val) {
  global $wpdb;
  return $wpdb->get_results("SELECT * FROM $table WHERE $where=".$val);
}
add_action('admin_menu', 'ratingSettingsMenuOptions');

// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// RATING SECTION CLIENT

function getRatingItems($data) {
  global $wpdb;

  // ACCOUNT
  $account = wp_get_current_user();
  $account_id = reset($account)->ID;
  
  // TABLES
  $rating_radio_table   = $wpdb->prefix . 'mj_rating_radios';
  $rating_items_table   = $wpdb->prefix . 'mj_rating_items';
  $rating_results_table = $wpdb->prefix . 'mj_rating_results';
  
  $json = stripslashes($_GET['data']);
  $result = json_decode($json,true);
  $post_id = $result['postId'];
  
  // GET RATING ITEMS 
  $arr = $wpdb->get_results("
    SELECT * 
    FROM $rating_items_table 
    WHERE post_id = 0
    OR post_id = $post_id
  ");

  if (array_key_exists('moduleId', $result)) {
    $arr = $wpdb->get_results("
      SELECT * 
      FROM $rating_items_table 
      WHERE id = $result[moduleId]
    ");
  }
   
  foreach ($arr as $key => $item) {
    $rating_id = $item->id;

    $a = (array) $item;
    $b = array();
    $c = array();
    
    // MERGE AVERAGE RATINGS
    if ($item->type_name == 'slider') {
      $sum = 0;
      $count = 0;
      $all_ratings = $wpdb->get_results("
        SELECT *
        FROM $rating_results_table
        WHERE rating_id = $rating_id
        AND post_id = $post_id
      ");
      foreach ($all_ratings as $rating) {
        $sum = $sum + $rating->result;
        $count++;
      }
      $b = (array) array('average_rating' => $sum/$count);
    }
    if ($item->type_name == 'radio') {
      $radioResults = array();

      $all_options = $wpdb->get_results("
        SELECT * 
        FROM $rating_radio_table
        WHERE rating_id = $rating_id
      ");
      
      foreach ($all_options as $option) {
        array_push($radioResults, array(
          'id'=> $option->id,
          'name' => $option->name,
          'average_rate' => 0
        ));
      }

      $all_ratings = $wpdb->get_results("
        SELECT *
        FROM $rating_results_table
        WHERE rating_id = $rating_id
        AND post_id = $post_id
      ");

      foreach ($radioResults as $idx => $target) {
        $sum = 0;
        foreach ($all_ratings as $rating) {
          if ($rating->result == $target['id']) {
            $sum++;
          }
        }
        $radioResults[$idx]['average_rate'] = $sum/count($all_ratings)*100;
      }

      $b = (array) array('average_rating' => $radioResults);
    }

    // MERGE USER RATINGS IF LOGGED IN
    if (is_user_logged_in()) {
      $user_rating = $wpdb->get_results("
        SELECT *
        FROM $rating_results_table
        WHERE user_id = $account_id
        AND rating_id = $rating_id
        AND post_id = $post_id
      ");
      $c = (array) array('user_rating' => reset($user_rating));
    }
    
    $arr[$key] = array_merge($a, $b, $c);
  }
  
  echo json_encode($arr,JSON_UNESCAPED_UNICODE);
  die;
}
function getRadioItems($data) {
  global $wpdb;
  $table = $wpdb->prefix.'mj_rating_radios';
  $json = stripslashes($_GET['data']);
  $result = json_decode($json,true);
  $arr = $wpdb->get_results("
    SELECT * 
    FROM $table 
    WHERE rating_id = " . $result['id']
  );
  echo json_encode($arr,JSON_UNESCAPED_UNICODE);
  die;
}
function putRatingResult($data) {
  global $wpdb;
  $table         = $wpdb->prefix.'mj_rating_results';  
  $account       = wp_get_current_user();
  $account_id    = reset($account)->ID;
  $json          = json_decode(stripslashes($_GET['data']),true);
  $rating_id     = $json['rating_id'];
  $post_id       = $json['post_id'];
  $result        = $json['result'];       
  $rating_result = $wpdb->get_results("
    SELECT *
    FROM $table
    WHERE user_id = $account_id
    AND rating_id = $rating_id
    AND post_id = $post_id
  ");
  echo $rating_result;
  if (empty($rating_result)) {
    $wpdb->insert( 
      $table,
      array(
        'post_id'   => $post_id,
        'user_id'   => $account_id,
        'rating_id' => $rating_id,
        'result'    => $result,
      )
    );
  } else {
    $wpdb->update( 
      $table,
      array(
        'result' => $result
      ),
      array(
        'post_id'   => $post_id,
        'user_id'   => $account_id,
        'rating_id' => $rating_id,
      )
    );
  }
  $arr = array(
    'state' => 'success',
    'msg' => 'rating updated..'
  );
  echo json_encode($arr,JSON_UNESCAPED_UNICODE);
  die();
}
add_action('wp_ajax_getRatingItems','getRatingItems');
add_action('wp_ajax_nopriv_getRatingItems','getRatingItems');
add_action('wp_ajax_getRadioItems','getRadioItems');
add_action('wp_ajax_nopriv_getRadioItems','getRadioItems');
add_action('wp_ajax_putRatingResult','putRatingResult');

// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
?>
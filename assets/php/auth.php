<?php
function logout() {
  check_ajax_referer( 'ajax-checkAuth-nonce', 'security' );
  wp_logout();
  wp_die();
}
function checkAuth() {
  check_ajax_referer('ajax-checkAuth-nonce','security');
  if (is_user_logged_in()) {
    $arr = array(
      'loggedIn' => true,
      'account' => wp_get_current_user()
    );
  } else {
    $arr = array(
      'loggedIn' => false,
      'account' => null
    );
  }
  echo json_encode($arr,JSON_UNESCAPED_UNICODE);
  wp_die();
}
function updateAuth() {
  check_ajax_referer('ajax-checkAuth-nonce','security');
  // https://codex.wordpress.org/Function_Reference/wp_update_user
  $json = stripslashes($_GET['data']);
  $result = json_decode($json,true);
  $user_id = intval($result['ID']);
  $user_email = $result['user_email'];
  // ERROR..
  if (email_exists($user_email)) {
    $arr = array(
      'state' => 'error',
      'msg' => 'Already existing email address.'
    );
  } 
  // SUCCESS..
  else {
    $userdata = array(
      'ID' => $user_id,
      'user_email' => $user_email
    );
    $arr = array(
      'state' => 'success',
      'msg' => 'Profile Updated.'
    );
    wp_update_user($userdata);
  }
  
  echo json_encode($arr,JSON_UNESCAPED_UNICODE);
  die();
}
function getVendorAccess() {
  $json = stripslashes($_GET['data']);
  $result = json_decode($json,true);
  $vendor = array(
    'name' => $result['vendor']
  );
  $code = $result['code'];
  $ch = curl_init();
  if ($vendor['name'] == 'vimeo') {
    $client_id = '399cceece445ae0887e56ee28b5fb65d05acbceb';
    $client_secret = 'FZuNCBUrgbC5CBoQ9BmVMHOe0PAlA3Vb372EKuMkFK8/F+h5R9PMCi9rfnYnWG+vB4z4R8+2i5+rlHhpe/qIoI6ooVH/mWmQim4aRAEcMvB0BG3nMIoouLQabk0GBuFn';
    $url = 'https://api.vimeo.com/oauth/access_token';
    $fields = array(
      'code' => $code,
      'grant_type' => 'authorization_code',
      'redirect_uri' => 'http://localhost:8888/moviejump.com/upload?provider=vimeo',
      'client_id' => $client_id,
      'client_secret' => $client_secret
    );
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
  }
  if ($vendor['name'] == 'youtube') {
    $url = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token='.$code;
  }
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $response = curl_exec($ch);
  curl_close($ch);
  echo $response;
  die();
}
add_action('wp_ajax_logout','logout');
add_action('wp_ajax_checkAuth','checkAuth');
add_action('wp_ajax_nopriv_checkAuth','checkAuth');
add_action('wp_ajax_updateAuth', 'updateAuth');
add_action('wp_ajax_getVendorAccess', 'getVendorAccess');
?>
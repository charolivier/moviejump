<?php
  function libs() {
    $blogurl = get_bloginfo('template_url');

    if (!is_admin()) {
      wp_deregister_script('jquery');

      // CSS
      wp_register_style('icons', $blogurl.'/assets/dist/font-awesome/css/font-awesome.min.css');
      wp_enqueue_style('icons');
      wp_register_style('css', $blogurl.'/assets/dist/css/app.min.css');
      wp_enqueue_style('css');

      // ANGULAR
      wp_register_script('angular',$blogurl.'/assets/dist/angular/angular.min.js',true);
      wp_enqueue_script('angular');
      wp_register_script('angular-sanitize',$blogurl.'/assets/dist/angular-sanitize/angular-sanitize.min.js',true);
      wp_enqueue_script('angular-sanitize');
      wp_register_script('angularRoute',$blogurl.'/assets/dist/angular-route/angular-route.min.js',true);
      wp_enqueue_script('angularRoute');
      wp_register_script('angular-animate',$blogurl.'/assets/dist/angular-animate/angular-animate.min.js',true);
      wp_enqueue_script('angular-animate');
      wp_register_script('angular-cookies',$blogurl.'/assets/dist/angular-cookies/angular-cookies.min.js',true);
      wp_enqueue_script('angular-cookies');
      
      // VENDOR
      wp_register_script('youtube','https://www.youtube.com/iframe_api',true);
      wp_enqueue_script('youtube');

      // APP
      wp_register_script('js',$blogurl.'/assets/dist/js/app.min.js',null,null);
      wp_localize_script('js','localize', array(
          'url' => array(
            'blog' => get_bloginfo('url'),
            'temp' => get_template_directory_uri() . '/',
            'ajax' => admin_url('admin-ajax.php')
          ),
          'nonce' => array(
            'checkAuth' => wp_create_nonce('ajax-checkAuth-nonce'),
            'rating' => wp_create_nonce('ajax-rating-nonce'),
          ),
          'terms' => array(
            'genres' => get_terms('genre', array(
					      'hide_empty' => false,
					      'orderby' => 'name',
					      'order' => 'ASC',
					  ))
          ),
        )
      );
      wp_enqueue_script('js');
    }
  };
  add_action('wp_enqueue_scripts','libs');
?>
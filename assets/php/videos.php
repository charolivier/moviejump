<?php
function uploadVideo() {
  // SECURITY NONCE CHECK..
  check_ajax_referer('ajax-checkAuth-nonce','security');

  $json = stripslashes($_GET['data']);
  $result = json_decode($json,true);

  // GET ALL POSTED DATA
  global $current_user;
  get_currentuserinfo();
  $user_id = $current_user->ID;
  $title = $result['title'];
  $description = $result['description'];
  $genre = explode(',', $result['genre']);
  $video_id   = $result['id'];
  $publishedAt = $result['publishedAt'];
  $thumbnail = $result['thumbnail'];
  $vendor  = $result['vendor'];

  // CHECK IF META ALREADY EXIST
  function checkURL($video_id,$vendor) {
    $args = array(
      'post_type'  => 'videos',
      'meta_query' => array(
        array(
          'key'   => 'wpcf-vidid',
          'value' => $video_id,
        ),
      ),
    );

    $vid_query = new WP_Query($args);

    if (empty($vid_query->posts)) {
      return false;
    } else {
      return true;
    }
  }
  if (checkURL($video_id)) {
    $arr = array(
      'state' => 'error',
      'msg' => 'An error append. This video already exist.',
    );
    
    echo json_encode($arr,JSON_UNESCAPED_UNICODE);
    
    die();
  }

  // ADD NEW POST TITLE + CONTENT + TAXONOMY 
  $post_id = wp_insert_post(array(
    'post_title' => $title,
    'post_content' => $description,
    'post_author' => $user_id,
    'tax_input' => array('genre' => $genre), // TAXONOMY
    'post_type' => 'videos', // CUSTOM POST-TYPE
    'post_status' => 'publish',
  ), false);

  // ADD VIDEO URL + ID + VENDOR
  update_post_meta($post_id,'wpcf-date',$publishedAt);
  update_post_meta($post_id,'wpcf-vidid',$video_id);
  update_post_meta($post_id,'wpcf-provider',$vendor);

  // ADD FEATURED IMG
  $upload_dir = wp_upload_dir();
  $image_data = file_get_contents($thumbnail);
  $filename = basename($thumbnail);
  if (wp_mkdir_p($upload_dir['path'])) {
    $file = $upload_dir['path'] . '/' . $filename;
  } else {
    $file = $upload_dir['basedir'] . '/' . $filename;
  }
  file_put_contents($file, $image_data);
  $wp_filetype = wp_check_filetype($filename, null);
  $attachment = array(
    'post_mime_type' => $wp_filetype['type'],
    'post_title' => sanitize_file_name($filename),
    'post_content' => '',
    'post_status' => 'inherit',
  );
  $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
  require_once(ABSPATH . 'wp-admin/includes/image.php');
  $attach_data = wp_generate_attachment_metadata($attach_id, $file);
  wp_update_attachment_metadata($attach_id, $attach_data);
  set_post_thumbnail($post_id, $attach_id);

  // SUCCESS
  if ($post_id != 0) {
    $arr = array(
      'state' => 'success',
      'msg' => 'Your video was successfully imported to Moviejump..'
    );
  }
  // ERROR
  else {
    $arr = array(
      'state' => 'error',
      'msg' => 'An error append. If you keep getting this message, feel free to contact us.'
    );
  }

  echo json_encode($arr,JSON_UNESCAPED_UNICODE);

  die;
}
function getVideos() {
  // RETURN ID
  global $current_user;
  get_currentuserinfo();
  $currentuser = $current_user->ID;
  $json = stripslashes($_GET['data']);
  $result = json_decode($json,true);
  
  // MY-CHANNEL VIDEO
  if ($result['type'] == 'myChannel') {
    $list = true;
    $args = array(
      'author'=> $currentuser,
      'post_type'=>'videos',
      'posts_per_page' => $result['postPerPage'],
      'offset' => $result['offset'],
    );
  }
  
  // BROWSE
  if ($result['type'] == 'browse') {
    $list = true;
    $args = array(
      'post_type'=>'videos',
      'posts_per_page' => $result['postPerPage'],
      'offset' => $result['offset'],
    );
  }
  
  // SINGLE
  if ($result['type'] == 'single') {
    $list = false;
    $args = array (
      'post_type'=>'videos',
      'post__in' => array($result['postId']),
    );
  }
  
  query_posts($args);

  // BUILD JSON  
  $arr = array();
  while (have_posts()):the_post();
  $foo= array();
  $id=get_the_id();
  $foo['id']=$id;
  $foo['title']=get_the_title();
  $foo['desc']=get_the_content();
  $foo['author']=get_the_author();
  $foo['vidid']=get_post_meta($id,'wpcf-vidid',true);
  $foo['vidpro']=get_post_meta($id,'wpcf-provider',true);
  $foo['thb']=wp_get_attachment_image_src(get_post_thumbnail_id($id),'medium')[0];
  $arr[]=$foo;
  endwhile;

  // RETURN POSTS
  echo json_encode($arr,JSON_UNESCAPED_UNICODE);

  // EXIT
  exit();
}
function deleteVideo() {
  // SECURITY NONCE CHECK..
  $nonce = $_POST['mjnonce'];
  if (! wp_verify_nonce($nonce,'myajax-mjnonce')){
    echo "An error occured. ".$vidtitle." was not deleted..";
    die ('Busted!');
  }
  
  //https://codex.wordpress.org/Function_Reference/wp_trash_post
  $vidtitle=$_POST['mjtitle'];
  $vidID=$_POST['mjid'];
  wp_trash_post($vidID);
  echo "".$vidtitle." deleted successfully..";
  
  // EXIT
  die();
}
add_action('wp_ajax_uploadVideo','uploadVideo');
add_action('wp_ajax_getVideos','getVideos');
add_action('wp_ajax_nopriv_getVideos','getVideos');
add_action('wp_ajax_deleteVideo','deleteVideo');
?>
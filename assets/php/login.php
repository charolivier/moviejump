<?php
function authStylesheet(){
  $blogurl=get_bloginfo('template_url');
  wp_register_style('custom',''.$blogurl.'/assets/dist/css/app.min.css',false,true);
  wp_enqueue_style('custom');
};
function authLogoUrl() {
    return home_url();
}
function authLogoTitle() {
  $name=get_bloginfo('name');
  $desc=get_bloginfo('description');
  return $name .' - '. $desc;
}
function authRedirect () {
  if (isset($_SERVER['HTTP_REFERER'])) {
    $url = $_SERVER['HTTP_REFERER'];
    $pattern = '@'.get_bloginfo('url').'@';
    preg_match($pattern, $url, $matches);
    if ($matches) {
      $pattern = '@login@';
      preg_match($pattern, $url, $matches);
      if (!$matches) {
        return $url;
      }
    }
  }
}
function getLoginUrl () {
  $json = stripslashes($_GET['data']);
  $result = json_decode($json,true);
  $url = $result['url'];
  echo wp_login_url($url);
  die();
}

add_action('login_enqueue_scripts', 'authStylesheet');
add_filter('login_headerurl', 'authLogoUrl');
add_filter('login_headertitle', 'authLogoTitle');
add_action('wp_ajax_getLoginUrl','getLoginUrl');
add_action('wp_ajax_nopriv_getLoginUrl','getLoginUrl');
?>
<?php
function userRedirect(){
	global $current_user;

  $user = wp_get_current_user();

  $home = get_bloginfo('url');

  if (is_admin() && $user->ID > 1) {
    // IF ADMIN IS AN AJAX CALL LET IT BE ELSE REDIRECT TO ACCOUNT..
    if (defined('DOING_AJAX') && DOING_AJAX) {
      // AJAX CALL, LET IT BE..
    } else {
      wp_redirect($home);
      exit;
    }
  }
};
function hideAdminBar() {
  show_admin_bar(false);
};

add_action('wp_loaded','userRedirect');
add_action('wp_loaded','hideAdminBar');
?>
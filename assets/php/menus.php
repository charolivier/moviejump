<?php
function register_my_menus() {
  register_nav_menus(array(
    "header-menu" => __( "header-menu" ),
    "footer-menu" => __( "footer-menu" ),
    "social-menu" => __("social-menu")
    )
  );
};
add_action("init","register_my_menus");

function li_class($classes, $item){
    $menu_locations = get_nav_menu_locations();
    if (has_term($menu_locations['header-menu'], 'nav_menu', $item)) {
      $classes[] = 'navbar-links-list-item';
    };
    if (has_term($menu_locations['footer-menu'], 'nav_menu', $item)) {
      $classes[] = 'footer-links-list-item';
    };
    return $classes;
}
add_filter('nav_menu_css_class' , 'li_class' , 10 , 2);

function a_class($output) {
  $output= preg_replace('/<a/', '<a class="link"', $output, -1);
  return $output;
};
add_filter('wp_nav_menu', 'a_class');
?>

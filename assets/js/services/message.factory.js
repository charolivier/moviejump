(function(){
  'use strict';

  angular
  .module('app')
  .factory('MessageService', MessageService);
  
  MessageService.$inject = ['$timeout'];
  
  function MessageService($timeout) {
    var service = {
      data: null,
      state: null,
      update: update
    };

    return service;

    function update(val,time) {
      if (val) {
        if (val.msg) {
          service.data = val.msg;
        }

        if (val.state) {
          service.state = val.state;
        }
      
        if (time) {
          $timeout(function(){
            service.data = null;
            service.state = null;
          },time);
        }
      }
    }
  }
})();
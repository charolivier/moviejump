(function(){
  'use strict';

  angular
  .module('app')
  .factory('AuthService', AuthService);

  AuthService.$inject = ['$http','$location','PageService'];

  function AuthService($http, $location, PageService) {
    var service = {
      login:           login,
      logout:          logout,
      update:          update,
      check:           check,
      requireLogin:    requireLogin,
      getVendorAuth:   getVendorAuth,
      getVendorToken:  getVendorToken,
      getVendorInfo:   getVendorInfo,
      user:            null
    };
    
    var api = {
      vimeo: 'https://api.vimeo.com',
      youtube: 'https://www.googleapis.com/youtube/v3'
    };

    return service;
    
    function logout(cb) {
      return $http({
        method: 'POST',
        url: localize.url.ajax,
        params: {
          action: 'logout',
          security: localize.nonce.checkAuth
        }
      })
      .then(function() {
        service.user = {
          loggedIn: false,
          account: null
        }
        if (cb == 'password') {
          PageService.redirect('./wp-login.php?action=lostpassword');
        } else {
          PageService.redirect('./')
        }
      });
    }
    
    function update(data) {
      return $http({
        method: 'POST',
        url: localize.url.ajax,
        params: {
          action: 'updateAuth',
          security: localize.nonce.checkAuth,
          data: data
        }
      })
      .then(
        function(response) {
          return response.data;
        }
      )
    }
    
    function login(url) {
      if (url) {
        $http({
          method: 'GET',
          url: localize.url.ajax,
          params: {
            action: 'getLoginUrl',
            data: {
              url: $location.absUrl()
            }
          }
        }).then(function(response){
          PageService.redirect(response.data);
        });
      } else {
        PageService.redirect('./wp-login.php');
      }
    }
    
    function check() {
      return $http({
        method: 'GET',
        url: localize.url.ajax,
        params: {
          action: 'checkAuth',
          security: localize.nonce.checkAuth
        }
      })
      .then(function(response) {
        service.user = response.data;
      })
    }

    function requireLogin() {
      PageService.redirect('./wp-login.php');
    }
    
    function getVendorAuth(vendor) {
      if (vendor == 'youtube') {
        var arg = {
          url:          'https://accounts.google.com/o/oauth2/auth',
          clientId:     '294756044536-gm19cjgh12scijaqqdho7h9pjngjshlk.apps.googleusercontent.com',
          redirect_uri: 'http%3A%2F%2Flocalhost%3A8888%2Fmoviejump.com%2Fupload%3Fprovider%3Dyoutube',
          apiScope:     'https://www.googleapis.com/auth/youtube',
          apiToken:     'token'
        }
        var url = arg.url+'?client_id='+arg.clientId+'&redirect_uri='+arg.redirect_uri+'&scope='+arg.apiScope+'&response_type='+arg.apiToken;
      }
      if (vendor == 'vimeo') {
        var arg = {
          url:           'https://api.vimeo.com/oauth/authorize',
          response_type: 'code',
          client_id:	   '399cceece445ae0887e56ee28b5fb65d05acbceb',
          redirect_uri:  'http%3A%2F%2Flocalhost%3A8888%2Fmoviejump.com%2Fupload%3Fprovider%3Dvimeo',
          scope:	       'public',
          state:	       'moviejump'
        }
        var url = arg.url+'?response_type='+arg.response_type+'&client_id='+arg.client_id+'&redirect_uri='+arg.redirect_uri+'&scope:'+arg.scope+'&state='+arg.state;
      }
      PageService.redirect(url);
    }
    
    function getVendorToken(vendor,code) {
      var data = {
        vendor: vendor,
        code: code
      }
      return $http({
        method: 'POST',
        url: localize.url.ajax,
        params: {
          action: 'getVendorAccess',
          data: data
        }
      })
      .then(function(response){
        return response.data;
      });
    }
    
    function getVendorInfo(vendor,token) {
      if (vendor == 'youtube') {
        var url = api.youtube + '/channels?part=id,contentDetails&mine=true&access_token=' + token;
      }

      if (vendor == 'vimeo') {
        var url = api.vimeo + '/me?access_token=' + token
      }
      
      return $http({
        method: 'GET',
        url: url
      })
      .then(function(response) {
        return response.data;
      })
    }
  }
})();
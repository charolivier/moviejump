(function(){
  'use strict';

  angular
  .module('app')
  .factory('ButtonService', ButtonService);
  
  ButtonService.$inject = ['$timeout'];
  
  function ButtonService($timeout) {
    var service = {
      data: {
        state: null,
        disabled: false
      },
      update: update,
      load: load
    };

    return service;

    function update(val) {
      service.data.state = 'loading';

      service.data.disabled = true;
      
      if (val) {
        if (val.state == 'error') {
          service.data.state = 'error';
        }

        if (val.state == 'success') {
          service.data.state = 'success';
        }
        
        $timeout(function(){
          service.data.state = null;
          service.data.disabled = false;
        }, 2000);
      }
    }

    function load() {
      service.data.state = 'loading';
      service.data.disabled = true;
    }
  }
})();
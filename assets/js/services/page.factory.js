(function(){
  'use strict';

  angular
  .module('app')
  .factory('PageService', PageService);
  
  PageService.$inject = ['$window'];

  function PageService($window) {
    var service = {
        redirect: redirect,
        setTitle: setTitle,
        title: null,
    };

    return service;

    function setTitle(newTitle) {
        service.title = newTitle;
    };
    
    function redirect($url) {
      $window.location.href = $url;
    }
  }
})();
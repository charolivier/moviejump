(function(){
  'use strict';

  angular
  .module('app')
  .factory('PopupService', PopupService);
  
  function PopupService() {
    var service = {
      val    : null,
      update : update,
      data   : {
        login: {
          id: 'login',
          slug: 'login',
          title: 'You must be logged in to start rating videos',
          content: 'If you dont have an account, you can create one in no time, it is free... if you already have one, you can login by clicking the link below.',
          btn: {
            primary: {
              slug: 'login',
              url: 'http://google.ca',
            }
          }
        }
      }
    };

    return service;

    function update(val) {
      if (val) {
        if (val == 'login') {
          data = data.login;
        } else {
          data = val;
        }
      }
    }
  }
})();
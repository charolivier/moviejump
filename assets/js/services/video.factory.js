(function(){
  'use strict';
  
  var youtubeApi = 'https://www.googleapis.com/youtube/v3/';

  angular
  .module('app')
  .factory('VideoService', VideoService);

  VideoService.$inject = ['$http'];

  function VideoService($http) {
    var service = {
      getVideos: getVideos,
      uploadVideo: uploadVideo,
      increment: increment,
      getVendorVideos: getVendorVideos
    };
    
    var api = {
      vimeo: 'https://api.vimeo.com',
      youtube: 'https://www.googleapis.com/youtube/v3'
    };

    return service;

    function getVideos(data) {
      return $http({
        method: 'GET',
        url: localize.url.ajax,
        params: {
          action: 'getVideos',
          data: data
        }
      })
      .then(function(response){
        return response.data;
      });
    }
    
    function getVendorVideos(data) {
      if (data.vendor=='youtube') {
        var url = api.youtube + '/playlistItems?part=snippet&playlistId=' + data.playListId + '&access_token=' + data.token;
      }
      if (data.vendor=='vimeo') {
        var url = api.vimeo + '/me/videos?filter_embeddable=true&access_token=' + data.token;
      }
      
      return $http({
        method: 'GET',
        url: url
      }).then(function(response){
        return response.data;
      })
    }
    
    function uploadVideo(data) {
      return $http({
        method: 'POST',
        url: localize.url.ajax,
        params: {
          action: 'uploadVideo',
          security: localize.nonce.checkAuth,
          data: data
        }
      })
      .then(function(response){
        return response.data;
      });
    }
    
    function increment(a,b) {
      if (a > 0) {
        a = a + b;
      } else {
        a = b;
      }
    
      return a;
    }
  }
})();
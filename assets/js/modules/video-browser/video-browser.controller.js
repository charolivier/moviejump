(function(){
  'use strict';

  angular
  .module('app')
  .controller('VideoBrowserController', VideoBrowserController);
    
  VideoBrowserController.$inject = ['PageService','VideoService'];

  function VideoBrowserController(PageService, VideoService) {
    var vm = this;
    
    vm.offset = 0;    
    vm.postPerPage = 20;    
    vm.more = true;
    vm.scrolly = true;
    vm.videos = [];
    
    PageService.setTitle('Browse');
    
    vm.getVideos = function() {
      if (vm.more && vm.scrolly) {
        var args = {
          type: 'browse',
          offset: vm.offset,
          postPerPage: vm.postPerPage
        }
        
        return VideoService.getVideos(args).then(function(data) {
          vm.videos = vm.videos.concat(data);

          vm.offset = VideoService.increment(vm.offset, vm.postPerPage);
          
          if ((data.length) < vm.postPerPage) {
            vm.more = false;
          }
          
          vm.scrolly = true;
        });
      }
    };
    
    vm.showMore = function() {
      vm.getVideos();
      vm.scrolly = false;
    }
    
    vm.getVideos();
  }
})();


(function(){
  'use strict';

  var modulesPath = localize.url.temp + 'assets/js/modules/';

  angular
  .module('app')
  .directive('ratingText', ratingText);

  function ratingText() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: modulesPath + 'rating-text/rating-text.view.html',
      controller: 'RatingTextController',
      controllerAs: 'ratingText',
      bindToController: true,
      scope: {
        module: '=',
        video:  '=',
        user: '='
      }
    }
  }
})();

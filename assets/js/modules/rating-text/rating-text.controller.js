(function(){
  'use strict';

  angular
  .module('app')
  .controller('RatingTextController', RatingTextController);

  RatingTextController.$inject = ['$scope','$http','AuthService'];

  function RatingTextController($scope,$http,AuthService) {
    var ratingText = this;
    
    ratingText.debug = false;

    ratingText.clickRate = function() {
      if (ratingText.user) {
        ratingText.nav=!ratingText.nav;
      } else {
        AuthService.login('redirect');
      }
    }
    
    ratingText.updateData = function(data) {
      return $http({
        method: 'POST',
        url: localize.url.ajax,
        params: {
          action: 'putRatingResult',
          security: localize.nonce.rating,
          data: {
            'post_id'   : ratingText.video.id,
            'rating_id' : ratingText.module.id,
            'result'    : data
          }
        }
      }).then(function(response){
        if (ratingText.debug) {
          console.log(response.data);
        }
      });
    }
    
    ratingText.clickRate = function() {
      if (ratingText.user) {
        ratingText.nav=!ratingText.nav
      } else {
        AuthService.login('redirect');
      }
    }
    
    $scope.$watch('ratingText.user', function(newVal){
      if (newVal) {
        ratingText.textarea = ratingText.module.user_rating.result || '';
      }
    })
  }
})();

(function(){
  'use strict';

  angular
  .module('app')
  .controller('MyChannelController', MyChannelController);
    
  MyChannelController.$inject = [
    'VideoService',
    'PageService',
    'AuthService',
    '$scope'
  ];

  function MyChannelController(
    VideoService,
    PageService,
    AuthService,
    $scope
  ) {

    var vm = this;
    
    // REQUIRE LOGIN
    $scope.$watch(
      function() {
        return AuthService.user;
      },
      function(newVal) {
        if (newVal && !newVal.loggedIn) {
          AuthService.requireLogin();
        }
      }
    );
    
    // PAGE TITLE
    PageService.setTitle('My Channel');
    
    vm.offset = 0;
    vm.postPerPage = 4;
    vm.more = true;
    vm.videos = [];
    
    vm.getVideos = function() {
      var arg = {
        type: 'myChannel',
        offset: vm.offset,
        postPerPage: vm.postPerPage
      }

      return VideoService.getVideos(arg).then(function(data) {
        vm.videos = vm.videos.concat(data);

        vm.offset = VideoService.increment(vm.offset, vm.postPerPage);
        
        if ((data.length) < vm.postPerPage) {
          vm.more = false;
        }
      });
    };

    vm.getVideos();
  }
})();

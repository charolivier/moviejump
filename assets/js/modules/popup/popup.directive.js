(function(){
  'use strict';

  var modulesPath = localize.url.temp + 'assets/js/modules/';

  angular
  .module('app')
  .directive('popupModule', popupModule);

  function popupModule() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: modulesPath + 'popup/popup.view.html',
      controller: 'PopupController',
      controllerAs: 'popup',
      bindToController: true,
      scope: {
        data: '='
      }
    }
  }
})();

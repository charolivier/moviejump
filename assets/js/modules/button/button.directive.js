(function(){
  'use strict';

  var modulesPath = localize.url.temp + 'assets/js/modules/';

  angular
    .module('app')
    .directive('btn', btn);
    
  function btn() {
      return {
        restrict: 'E',
        replace: true,
        templateUrl: modulesPath + 'button/button.view.html',
        controller: 'ButtonController',
        controllerAs: 'vm',
        scope: {
          title: '@',
          class: '@',
          event: '&'
        }
      }
  }
})();
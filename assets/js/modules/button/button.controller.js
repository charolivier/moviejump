(function(){
  'use strict';

  angular
    .module('app')
    .controller('ButtonController', ButtonController);
    
  ButtonController.$inject = ['$scope','ButtonService'];

  function ButtonController($scope, ButtonService) {
    var vm = this;
    
    $scope.$watch(
      function() {
        return ButtonService.data.state
      },
      function() {
        vm.buttonState = ButtonService.data.state;
        vm.buttonDisabled = ButtonService.data.disabled;
      }
    );
  }
})();




  
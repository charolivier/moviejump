(function(){
  'use strict';

  var modulesPath = localize.url.temp + 'assets/js/modules/';

  angular
  .module('app')
  .directive('message', message);

  function message() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: modulesPath + 'message/message.view.html',
      controller: 'MessageController',
      controllerAs: 'vm',
      bindToController: true
    }
  }
})();
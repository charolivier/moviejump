(function(){
  'use strict';

  angular
    .module('app')
    .controller('MessageController', MessageController);
    
  MessageController.$inject = ['$scope','MessageService'];

  function MessageController($scope, MessageService) {
    var vm = this;

    $scope.$watch(
      function() {
        return MessageService.data
      },
      function() {
        vm.message = MessageService.data;
        vm.messageState = MessageService.state
      }
    );
    
    vm.messageClose = function() {
      MessageService.data = null;
      MessageService.state = null;
    }
  }
})();




  
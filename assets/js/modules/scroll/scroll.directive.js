(function(){
  'use strict';
  
  angular
  .module('app')
  .directive('scrolly', scrolly);
  
  function scrolly() {
    return {
      restrict: 'A',
      link: function(scope, elem, attrs) {
        var raw = elem[0];
        
        elem.bind('scroll',function(event) {
          if ((raw.scrollTop + raw.offsetHeight) > (raw.scrollHeight/3)) {
            scope.$apply(attrs.scrolly);
          }
        })
      }
    }
  }
})();


(function(){
  'use strict';

  angular
    .module('app')
    .controller('VideoDetailsController', VideoDetailsController);

  VideoDetailsController.$inject = ['PageService', 'VideoService', '$location'];

  function VideoDetailsController(PageService, VideoService, $location) {
    var vm = this;
    
    vm.debug = false;

    vm.getVideo = function() {
      var args = {
        type: 'single',
        postId: $location.absUrl().split('=').pop()
      }
      
      return VideoService.getVideos(args).then(function(data) {
        vm.video = data[0];
        if (vm.debug) {
          console.log(vm.video);
        }
      });
    };
    
    PageService.setTitle('Detail');
    
    vm.getVideo();
  }
})();

(function(){
  'use strict';

  angular
  .module('app')
  .controller('UploadController', UploadController);

  UploadController.$inject = [
    'AuthService',
    'VideoService',
    'ButtonService', 
    'MessageService', 
    'PageService', 
    '$timeout',
    '$location',
    '$cookies',
    '$scope'
  ];

  function UploadController(
    AuthService,
    VideoService,
    ButtonService,
    MessageService,
    PageService,
    $timeout,
    $location,
    $cookies,
    $scope
  ) {

    var vm = this;

    function isEmpty(myObject) {
      for(var key in myObject) {
        if (myObject.hasOwnProperty(key)) {
          return false;
        }
      }

      return true;
    }

    // REQUIRE LOGIN

    $scope.$watch(
      function() {
        return AuthService.user;
      },
      function(newVal) {
        if (newVal && !newVal.loggedIn) {
          AuthService.requireLogin();
        }
      }
    );

    // VENDOR VIDEOS
    
    vm.debug = false;

    vm.vendorVideos = null;

    vm.urlParam = null;

    vm.selectVendor = function(vendor) {
      var token = $cookies.get(vendor+'AccessToken');
  
      if (token) {
        vm.getVendorInfo(vendor,token);
      } else {
        AuthService.getVendorAuth(vendor);
      }
    }

    vm.getUrlParam = function() {
      if (vm.debug) {
        console.log('getUrlParam..');
      }

      vm.urlParam = $location.search();

      if ($location.hash()) {
        $location.hash().split('&').map(function(str){
          var arr = str.split('=');
          var key = arr.slice(0,1);
          var val = arr.pop();
          vm.urlParam[key] = val;
        })
      }
      
      return vm.urlParam;
    }

    vm.getVendorToken = function(data) {
      if (vm.debug) {
        console.log('getVendorToken..');
      }
      
      $location.url($location.path()); 

      if (data.provider == 'youtube') {
        vm.vendor = 'youtube';
        vm.code = data.access_token;
      }

      if (data.provider == 'vimeo') {
        vm.vendor = 'vimeo';
        vm.code = data.code;
      }
      
      AuthService.getVendorToken(vm.vendor,vm.code)
      .then(function(data){
        if (vm.vendor == 'youtube') {
          $cookies.put('youtubeAccessToken', vm.code);
          vm.getVendorInfo(vm.vendor,vm.code);
        }
        if (vm.vendor == 'vimeo') {
          if (data.access_token) {
            $cookies.put('vimeoAccessToken', data.access_token);
            vm.getVendorInfo(vm.vendor,data.access_token);
          }
        }
      })
    }

    vm.getVendorInfo = function(vendor,token) {
      if (vm.debug) {
        console.log('vm.getVendorInfo..');
      }

      AuthService.getVendorInfo(vendor,token)
      .then(function(data){
        if (vm.debug) {
          console.log('getVendorInfo success..');
        }
        
        var json = {
          vendor: vendor,
          vendorInfo: data,
          token: token
        };
        vm.getVendorVideos(json);
      })
      .catch(function(error){
        if (vm.debug) {
          console.log('getVendorInfo error..');
        }
        $cookies.remove(vendor+'AccessToken');
        AuthService.getVendorAuth(vendor);
      })
    }

    vm.getVendorVideos = function(data) {
      if (vm.debug) {
        console.log('getVendorVideos..');
      }
      var vendor = data.vendor;
      if (vendor == 'vimeo') {
        var arr = {
          vendor: 'vimeo',
          token: data.token
        }
      }
      if (vendor == 'youtube') {
        var arr = {
          vendor: 'youtube',
          playListId: data.vendorInfo.items[0].contentDetails.relatedPlaylists.uploads,
          token: data.token
        }
      }
      VideoService.getVendorVideos(arr).then(function(data){
        if (vm.debug) {
          console.log('getVendorVideos success..');
          console.log(data);
        }
        vm.vendorVideos = [];
        if (vendor == 'youtube') {
          data.items.map(function(obj){
            if (!isEmpty(obj.snippet)) {
              if (obj.snippet.thumbnails.standard) {
                var thumbnail = obj.snippet.thumbnails.standard.url;
              } else {
                var thumbnail = obj.snippet.thumbnails.high.url
              }
              
              vm.vendorVideos.push({
                'id': obj.snippet.resourceId.videoId,
                'title': obj.snippet.title,
                'thumbnail': thumbnail,
                'publishedAt': obj.snippet.publishedAt.split('T').slice(0,1).pop(),
                'description': obj.snippet.description,
                'vendor': 'youtube'
              });
            }
          });
        }
        if (vendor == 'vimeo') {
          if (!isEmpty(data.data)) {
            data.data.map(function(obj){
              if (obj.pictures.sizes[4]) {
                var thumbnail = obj.pictures.sizes[4].link;
              } else {
                var thumbnail = obj.pictures.sizes[3].link;
              }
              
              vm.vendorVideos.push({
                'id': obj.uri.replace('/video/',''),
                'title': obj.name,
                'thumbnail': thumbnail,
                'publishedAt': obj.created_time,
                'description': obj.description,
                'vendor': 'vimeo'
              });
            });
          }
        }
      })
    }

    if (!isEmpty(vm.getUrlParam())) {
      vm.getVendorToken(vm.getUrlParam());
    }

    // SELECTED VIDEO

    vm.selectVideo = function(video) {
      if (vm.debug) {
        console.log(video);
      }
      vm.video = video;
    }

    vm.genres = localize.terms.genres;
    
    vm.validForm = function(data) {
      if (!data.description.length) {
        var error = {
          state:'error',
          msg:'You forgot to enter a description..'
        };
      }

      if (data.genre == 'default') {
        var error = {
          state:'error',
          msg:'You forgot to categorize your video in the genre section..'
        };
      }

      if (!data.title.length) {
        var error = {
          state:'error',
          msg:'You forgot to enter a title..'
        };
      }

      if (error) {
        ButtonService.update(error);

        MessageService.update(error,5000);

        return false;
      } else {
        return true;
      }
    }

    vm.uploadVideo = function(data) {
      ButtonService.update();

      if (vm.validForm(data)) {
        VideoService.uploadVideo(data).then(function(data){
          ButtonService.update(data);
          MessageService.update(data,5000);
          // REDIRECT TO MY CHANNEL IF SUCCESS
          if (data.state == 'success') {
            $timeout(function(){
              $location.path('my-channel');
            },2000);
          }
        });
      }
    }

    vm.cancelUpload = function() {
      vm.video = null;
    }

  }
})();

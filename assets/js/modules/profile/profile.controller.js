(function(){
  'use strict';

  angular
  .module('app')
  .controller('ProfileController', ProfileController);

  ProfileController.$inject = ['AuthService','ButtonService','MessageService','PageService','$scope'];

  function ProfileController(AuthService, ButtonService, MessageService, PageService, $scope) {
    var vm = this;
    
    PageService.setTitle('Profile');
    
    $scope.$watch(
      function() {
        return AuthService.user;
      },
      function(newVal) {
        if (newVal.loggedIn) {
          vm.user = AuthService.user.account.data
        } else {
          AuthService.requireLogin();
        }
      }
    );
    
    vm.resetPassword = function() {
      AuthService.logout('password');
    };
    
    vm.updateProfile = function(user) {
      ButtonService.update();  
      AuthService.update(angular.toJson(user)).then(function(data) {
        ButtonService.update(data);
        MessageService.update(data,5000);
      });
    };
  }
})();
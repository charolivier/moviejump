(function(){
  'use strict';

  var modulesPath = localize.url.temp + 'assets/js/modules/';

  angular
  .module('app')
  .directive('ratingModules', ratingModules);

  function ratingModules() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: modulesPath + 'rating-modules/rating-modules.view.html',
      controller: 'RatingController',
      controllerAs: 'rating',
      bindToController: true,
      scope: {
        video: '=',
        user: '='
      }
    }
  }
})();

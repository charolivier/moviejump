(function(){
  'use strict';

  angular
  .module('app')
  .controller('RatingController', RatingController);

  RatingController.$inject = ['$scope','$http'];

  function RatingController($scope,$http) {
    var rating = this;
    
    rating.getRatingItems = function() {
      return $http({
        method: 'GET',
        url: localize.url.ajax,
        params: {
          action: 'getRatingItems',
          security: localize.nonce.rating,
          data: {
            postId: rating.video.id,
          }
        }
      }).then(function(response){
        rating.modules = response.data;
      });
    }
    
    rating.getRatingItems();
  }
})();

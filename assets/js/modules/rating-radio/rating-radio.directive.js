(function(){
  'use strict';

  var modulesPath = localize.url.temp + 'assets/js/modules/';

  angular
  .module('app')
  .directive('ratingRadio', ratingRadio);

  function ratingRadio() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: modulesPath + 'rating-radio/rating-radio.view.html',
      controller: 'RatingRadioController',
      controllerAs: 'ratingRadio',
      bindToController: true,
      scope: {
        module: '=',
        video:  '=',
        user: '='
      }
    }
  }
})();

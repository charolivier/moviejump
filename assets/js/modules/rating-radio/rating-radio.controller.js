(function(){
  'use strict';

  angular
  .module('app')
  .controller('RatingRadioController', RatingRadioController);

  RatingRadioController.$inject = ['$scope','$http','AuthService','PopupService'];

  function RatingRadioController($scope,$http,AuthService,PopupService) {
    var ratingRadio = this;
    
    ratingRadio.debug = false;
    
    ratingRadio.nav = false;
    
    ratingRadio.getData = function() {
      return $http({
        method: 'GET',
        url: localize.url.ajax,
        params: {
          action: 'getRatingItems',
          security: localize.nonce.rating,
          data: {
            postId: ratingRadio.video.id,
            moduleId: ratingRadio.module.id
          }
        }
      }).then(function(response){
        ratingRadio.module = response.data.pop();
        ratingRadio.nav = false;
      });
    }

    ratingRadio.getRadio = function() {
      return $http({
        method: 'GET',
        url: localize.url.ajax,
        params: {
          action: 'getRadioItems',
          data: {
            id: ratingRadio.module.id
          }
        }
      }).then(function(response){
        ratingRadio.radios = response.data;
      });
    }
    
    ratingRadio.updateData = function(data) {
      return $http({
        method: 'POST',
        url: localize.url.ajax,
        params: {
          action: 'putRatingResult',
          security: localize.nonce.rating,
          data: {
            'post_id'   : ratingRadio.video.id,
            'rating_id' : data.rating_id,
            'result'    : data.id
          }
        }
      }).then(function(){
        ratingRadio.getData();
      });
    }
    
    ratingRadio.clickRadio = function(data) {
      ratingRadio.selected = data.id;
      ratingRadio.updateData(data);
    }
    
    ratingRadio.clickRate = function() {
      if (ratingRadio.user) {
        ratingRadio.nav=!ratingRadio.nav
      } else {
        PopupService.update('login');
        // AuthService.login('redirect');
      }
    }
    
    $scope.$watch('ratingRadio.user', function(newVal) {
      if (newVal) {
        ratingRadio.selected = ratingRadio.module.user_rating.result || null;
      }
    });
    
    ratingRadio.getRadio();
  }
})();

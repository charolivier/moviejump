(function(){
  'use strict';

  var modulesPath = localize.url.temp + 'assets/js/modules/';

  angular
  .module('app')
  .directive('ratingSlider', ratingSlider);

  function ratingSlider() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: modulesPath + 'rating-slider/rating-slider.view.html',
      controller: 'RatingSliderController',
      controllerAs: 'ratingSlider',
      bindToController: true,
      scope: {
        module: '=',
        video:  '=',
        user: '='
      }
    }
  }
})();

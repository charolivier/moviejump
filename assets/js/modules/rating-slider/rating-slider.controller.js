(function(){
  'use strict';

  angular
  .module('app')
  .controller('RatingSliderController', RatingSliderController);

  RatingSliderController.$inject = ['$scope','$http','$timeout','AuthService'];

  function RatingSliderController($scope,$http,$timeout,AuthService) {
    var ratingSlider = this;

    ratingSlider.initializing = true;
    
    ratingSlider.debug = false;
    
    ratingSlider.nav = false;
    
    if (!ratingSlider.module.average_rating) {
      ratingSlider.module.average_rating = 50;
    }
    
    if (ratingSlider.debug) {
      console.log(ratingSlider);
    }
    
    ratingSlider.getData = function() {
      return $http({
        method: 'GET',
        url: localize.url.ajax,
        params: {
          action: 'getRatingItems',
          security: localize.nonce.rating,
          data: {
            postId: ratingSlider.video.id,
            moduleId: ratingSlider.module.id
          }
        }
      }).then(function(response){
        ratingSlider.module = response.data.pop();
        ratingSlider.nav = false;
      });
    }
    
    ratingSlider.updateData = function(newValue) {
      return $http({
        method: 'POST',
        url: localize.url.ajax,
        params: {
          action: 'putRatingResult',
          security: localize.nonce.rating,
          data: {
            'post_id'   : ratingSlider.video.id,
            'rating_id' : ratingSlider.module.id,
            'result'    : newValue
          }
        }
      }).then(function(){
        ratingSlider.getData();
      });
    }
    
    ratingSlider.clickRate = function() {
      if (ratingSlider.user) {
        ratingSlider.nav=!ratingSlider.nav
      } else {
        AuthService.login('redirect');
      }
    }
    
    $scope.$watch('ratingSlider.user', function(newVal){
      if (newVal) {
        ratingSlider.sliders = [
          {
            title:'Score: ', 
            value: ratingSlider.module.user_rating.result || 50
          }
        ];

        $scope.$watch(
          function() {
            return ratingSlider.sliders[0].value
          },
          function(newValue) {
            if (ratingSlider.initializing) {
              $timeout(function() {
                ratingSlider.initializing = false;
              });
            } else {
              $timeout(function(){
                if (newValue == ratingSlider.sliders[0].value) {
                  ratingSlider.updateData(newValue);
                }
              },1500)
            }
          }
        );
      }
    });
  }
})();

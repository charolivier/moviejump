(function(){
  'use strict';

  angular
  .module('app')
  .controller('VideoPlayerController', VideoPlayerController);

  VideoPlayerController.$inject = ['$scope','$sce'];

  function VideoPlayerController($scope,$sce) {
    var player = this;
    
    $scope.$watch('player.vidid', function(newVal){
      if (newVal) {
        if (player.vidid && player.vidpro == 'youtube') {
          player.vidsrc = $sce.trustAsResourceUrl('http://www.youtube.com/embed/'+player.vidid+'?showinfo=0&showsearch=0&iv_load_policy=3');
        }
        if (player.vidid && player.vidpro == 'vimeo') {
          player.vidsrc = $sce.trustAsResourceUrl('https://player.vimeo.com/video/'+player.vidid+'?title=0&byline=0&portrait=0');
        }
      }
    });
  }
})();

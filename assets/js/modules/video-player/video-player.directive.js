(function(){
  'use strict';

  var modulesPath = localize.url.temp + 'assets/js/modules/';

  angular
  .module('app')
  .directive('videoPlayer', VideoPlayer);

  function VideoPlayer() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: modulesPath + 'video-player/video-player.view.html',
      controller: 'VideoPlayerController',
      controllerAs: 'player',
      bindToController: true,
      scope: {
        vidid:  "@",
        vidpro: "@"
      }
    }
  }
})();
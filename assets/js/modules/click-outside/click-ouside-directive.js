(function(){
  'use strict';
  
  var modulesPath = localize.url.temp + 'assets/js/modules/';
  
  angular
  .module('app')
  .directive('clickOutside', clickOutside);

  clickOutside.$inject = ['$document','SideNavService'];

  function clickOutside($document, SideNavService) {
    return {
      restrict: 'A',
      link: function(scope, elem) {
        elem.bind('click', function(e) {
          scope.state = true;
          SideNavService.showSideNav();
        });
        
        $document.bind('click', function() {
          if (!scope.state) {
            SideNavService.hideSideNav();
          }
          scope.state = false;
        });
        
        
      }
    }
  } 
})();
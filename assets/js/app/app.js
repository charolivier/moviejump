(function(){
  'use strict';
  
  var modulesPath = localize.url.temp + 'assets/js/modules/';
  
  angular
  .module('app', ['ngRoute','ngSanitize','ngAnimate','ngCookies','truncate','angularMultiSlider'])
  .controller('AppController', AppController)
  .config(config);

  config.$inject = ['$routeProvider','$locationProvider'];

  function config($routeProvider, $locationProvider) {
    var modulesPath = localize.url.temp + 'assets/js/modules/';

    $locationProvider.html5Mode(true);

    $routeProvider
    .when('/', {
      templateUrl:  modulesPath + 'video-browser/video-browser.view.html',
      controller:   'VideoBrowserController',
      controllerAs: 'vm'
    })
    .when('/profile', {
      templateUrl:  modulesPath + 'profile/profile.view.html',
      controller:   'ProfileController',
      controllerAs: 'vm'
    })
    .when('/my-channel', {
      templateUrl:  modulesPath + 'my-channel/my-channel.view.html',
      controller:   'MyChannelController',
      controllerAs: 'vm'
    })
    .when('/upload', {
      templateUrl:  modulesPath + 'upload/upload.view.html',
      controller:   'UploadController',
      controllerAs: 'vm',
      reloadOnSearch: false,
      reloadOnHash: false
    })
    .when('/watch', {
      templateUrl:  modulesPath + 'video-details/video-details.view.html',
      controller:   'VideoDetailsController',
      controllerAs: 'vm'
    })
    .otherwise({
      redirectTo: '/'
    });
  }

  AppController.$inject = [
    'PageService', 
    'AuthService', 
    'PopupService', 
    '$location', 
    '$scope', 
    '$http', 
    '$route'
  ];

  function AppController(
    PageService, 
    AuthService,
    PopupService, 
    $location, 
    $scope, 
    $http, 
    $route
  ) {

    var app = this;
    
    var url = $location.path();
    
    app.debug = false;
    
    app.popup = {
      val: null
    };
    
    app.login = function() {
      AuthService.login();
    }
    
    app.logout = function() {
      AuthService.logout();
    }
    
    app.admin = function() {
      PageService.redirect('./wp-admin');
    }

    // SIDE NAV
    app.sideNav = {
      showing: null,
      show: function() {
        app.sideNav.showing = true;
        app.showSideNav = !app.showSideNav;
        app.showSideNav ? app.slide = 'slide' : app.slide = '';
      },
      hide: function() {
        if (!app.sideNav.showing) {
          app.showSideNav = false; 
          app.slide = '';
        }
        app.sideNav.showing = null;
      }
    }
    
    // PAGE TITLE
    $scope.$watch(
      function() {
        return PageService.title;
      },
      function(newVal) {
        if (newVal) {
          app.pageTitle = newVal;
        }
      }
    );
    
    // USER AUTH
    $scope.$watch(
      function() {
        return AuthService.user;
      },
      function(newVal) {
        if (newVal) {
          app.user = newVal;

          if (app.debug) {
            console.log(newVal);
          }
        }
      }
    );
    
    // POPUP
    $scope.$watch(
      function() {
        return PopupService.val;
      },
      function(newVal) {
        if (newVal) {
          app.popup.val = newVal;
        }
      }
    );
    
    // CURRENT URL
    $scope.$on(
      '$routeChangeStart', 
      function(next, current) { 
        app.url = $location.absUrl();
        if (app.debug) {
          console.log(app.url);
        }
      }
    );
    
    PageService.setTitle('Browse');
    
    AuthService.check();
  }
})();
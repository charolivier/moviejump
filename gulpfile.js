// Load gulp plugins with 'require' function of nodejs
var gulp       = require('gulp');
var plumber    = require('gulp-plumber');
var gutil      = require('gulp-util');
var uglify     = require('gulp-uglify');
var concat     = require('gulp-concat');
var rename     = require('gulp-rename');
var minifyCSS  = require('gulp-minify-css');
var path       = require('path');
var livereload = require('gulp-livereload');
var sass       = require('gulp-sass');
var ftp        = require('vinyl-ftp');
var sassGlob   = require('gulp-sass-glob');


// Handle less error
var onError = function (err) {
  gutil.beep();
  console.log(err);
};

// Path configs
var css_files  = './assets/css/*.css'; // .css files
var css_path   = './assets/css'; // .css path
var js_files   = './assets/js/**/*.js'; // .js files
var sass_file  = './assets/sass/**/*.scss'; // .scss files
var dist_path  = './assets/dist';
var bower_path = './assets/bower';
var bower_file = './assets/bower/**/*';

// Extension config
var extension = 'php';

// Functions for tasks
function jsTask() {
  return gulp.src(js_files)
  .pipe(plumber({
    errorHandler: onError
  }))
  .pipe(concat('dist'))
  .pipe(rename('app.min.js'))
  //.pipe(uglify())
  .pipe(gulp.dest(dist_path+"/js"))
  .pipe(livereload());
};
function cssTask() {
  return gulp.src(css_files)
  .pipe(concat('dist'))
  .pipe(rename('app.min.css'))
  .pipe(minifyCSS())
  .pipe(gulp.dest(dist_path+"/css"))
  .pipe(livereload());
};
function sassTask(err) {
  return gulp
  .src(sass_file)
  .pipe(sassGlob())
  .pipe(plumber({
    errorHandler: onError
  }))
  .pipe(sass())
  .pipe(gulp.dest(css_path))
  .pipe(livereload());
};
function vendorTask() {
  return gulp
    .src([
      bower_path + '/font-awesome/fonts/**/*',
      bower_path + '/font-awesome/css/font-awesome.min.css',
      bower_path + '/angular/angular.min.js',
      bower_path + '/angular-route/angular-route.min.js',
      bower_path + '/jquery/dist/jquery.min.js',
      bower_path + '/angular-animate/angular-animate.min.js',
      bower_path + '/angular-cookies/angular-cookies.min.js',
      bower_path + '/angular-sanitize/angular-sanitize.min.js'
    ],{base: bower_path})
    .pipe(gulp.dest(dist_path));
};
function reloadBrowser() {
  return gulp.src('*.' + extension)
  .pipe(livereload());
};

// default
gulp.task('default', function() {
  livereload.listen();

  gulp.watch(bower_file, function(){
    return vendorTask();
  })

  gulp.watch(sass_file, function() {
    return sassTask();
  });

  gulp.watch(css_files, function() {
    console.log('CSS task completed!');
    return cssTask();
  });

  gulp.watch(js_files, function() {
    console.log('JS task completed!');
    return jsTask();
  });

  gulp.watch('**/*.' + extension, function(){
    console.log('Browse reloaded!');
    return reloadBrowser();
  });
});

// build
gulp.task('build', function() {
  vendorTask();
  sassTask();
  cssTask();
  jsTask();
});

// deploy
gulp.task('deploy', function() {
  var creds = require('secrets');
  var connect = ftp.create({
    host: creds.host,
    user: creds.user,
    password: creds.password,
    parallel: 10,
    log: gutil.log
  });
  var globs = [
    '*.php',
    '*.js',
    './assets/dist/**/*',
    './assets/vendor/**/*',
    './images/*',
    './templates/*'
  ];
  return gulp.src(globs, {base:'.', buffer:false})
    // only upload newer files
    .pipe(connect.newer('domains/moviejump.miraculouscode.com/html/wp-content/themes/moviejump/')) 
    .pipe(connect.dest('domains/moviejump.miraculouscode.com/html/wp-content/themes/moviejump/'));
});


<!DOCTYPE html>

<html <?php language_attributes(); ?> ng-app="app" ng-controller="AppController as app">

<head>
  <meta charset="<?php bloginfo('charset'); ?>" />
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1" />
  <title>moviejump - {{ app.pageTitle }}</title>
  <base href="<?php echo get_bloginfo('url'); ?>/">
  <?php wp_head(); ?>
</head>

<body ng-click='app.sideNav.hide()' ng-class='app.slide'>
<header class="navbar">
  <div class="navbar-container">
    <div class="navbar-action left">
      <a 
      class="navbar-brand-link" 
      href="<?php bloginfo("url"); ?>">
        <img 
        class="navbar-brand-image" 
        alt="<?php bloginfo('name'); ?>" 
        src="<?php bloginfo('template_url'); ?>/assets/img/logo-color.png" />
      </a>
    </div>
    <div class="navbar-action right">
      <div class="navbar-links">
        <ul class="navbar-link-list" id="menu-header-menu">
          <div ng-if="app.user.loggedIn">
            <li class="navbar-links-list-item">
              <a ng-click='app.sideNav.show()' class="link link-account">
                <i class="icon-menu fa fa-bars"></i>
              </a>
            </li>
          </div>
          <div ng-if="!app.user.loggedIn">
            <li class="navbar-links-list-item">
              <a 
              class="link link-upload btn primary"
              ng-click="app.login()">Login</a>
            </li>
          </div>
        </ul>
      </div>
    </div>
  </div>
</header>
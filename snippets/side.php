<aside class='side-nav' ng-if='app.showSideNav'>
  <a href='./' class='logo'>
    <img class="navbar-brand-image" alt="<?php bloginfo('name'); ?>" 
    src="<?php bloginfo('template_url'); ?>/assets/img/logo-white.png" />
  </a>
  <a href='./profile'>
    <i class='fa fa-smile-o'></i>
    <span>profile</span>
  </a>
  <a href='./my-channel'>
    <i class='fa fa-film'></i>
    <span>my channel</span>
  </a>
  <a href='./upload'>
    <i class='fa fa-cloud-upload'></i>
    <span>upload</span>
  </a>
  <a ng-click='app.logout()'>
    <i class='fa fa-sign-out'></i>
    <span>Logout</span>
  </a>
  <?php if (reset(wp_get_current_user()->roles) == 'administrator'): ?>
    <a ng-click='app.admin()'>
      <i class='fa fa-lock'></i>
      <span>admin</span>
    </a>
  <?php endif; ?>
</aside>